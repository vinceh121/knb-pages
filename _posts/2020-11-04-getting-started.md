---
layout: post
title: Getting Started
author: vinceh121
date: 2020-11-02 08:05:00 +0100
pin: true
---

Kdecole Notification Bot is a bot that allows you to get notifications from your VLE directly into a Discord channel of your choice. The bot can inform you about new articles, emails and grades.

To add the bot, press on the Add To Discord tab on the left.

Once the bot is added, link your account using mobile access using your username and temporary password with this command: `@Kdecole Bot#6747 auth username password`

For additional help you can use the help command: `@Kdecole Bot#6747 help`<br>
Or if you're really stuck you can [contact me](https://vinceh121.me/contact)
