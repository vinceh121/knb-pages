---
layout: post
title: Privacy Policy
date: 2021-03-23
---

The following Privacy Policy ("Policy") concerns the Discord Bot "Kdecole Notification Bot" ("Bot") managed and administered by Vincent "vinceh121" Hyvert.

VLE means an instance of a Kdecole or Skolengo Virtual Learning Environment developped by Kosmos.

User means a Discord User.

An Integration is when a User links their VLE account to a Discord Channel using the Bot.

# Usage of Discord Data

This Bot respects the [Discord Developer Policy](https://discord.com/developers/docs/policy) and saves the following data provided by Discord when a User creates an Integration:

 - Guild ID of where the Integration was added
 - Channel ID of where the Integration was added
 - User ID of the User who added the Integration

This data is kept as long as the User does not logout of the Integration.

# Usage of VLE data

This Bot saves the following data provided by the VLE:

 - URL of the Mobile API endpoint of the VLE
 - Mobile token used to authenticate the User's VLE account

The data relayed by the Bot from the VLE to a Discord Channel is not saved and only processed immediatly.

The bot will NEVER make actions as the User with their VLE account and only fetch data as the Integration is configured.

This data is kept as long as the User does not logout of the Integration.

## Logging into a VLE account

In order to loginto a VLE account, the User needs to provide their VLE username and a temporary mobile access password. Those are not saved and only used when attempting to initialize mobile access. When VLE account logon is successful, the mobile access token is saved.

# Extra logged data

This Bot saves the following data in order to secure against potential attacks and help fix potential bugs:

 - Errors when logging into a VLE account
 - Errors when fetching VLE data
 - Timestamp, name and Guild ID of when a Bot is added/removed to/from a Guild

This data is deleted when it is older than 2 months

# Anonymized metrics data

This Bot saves the following anonymized data for metrics purposes:

 - Average count of VLE articles of all instances

# User data request

A User can fetch the data kept about them in a machine-readable format and will be provided ASAP through Discord by Direct Messages.

The data sent is the raw Integrations data as they are stored in the Bot's database. The Integrations exported are the ones created by the User.

# Data location

All data is stored in France. Our hosting provider is OVH SAS - 2 rue Kellermann, 59100 Roubaix, France. Datacenter location: 9 Rue du Bassin de l'Industrie, 67000 Strasbourg, France.

